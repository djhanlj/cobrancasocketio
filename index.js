var express = require("express");
var socketio = require("socket.io");
var bodyParser = require("body-parser");
var app = express();

//Produção
var https, server;
https = require("https");

const fs = require('fs');
const options = {
  key: fs.readFileSync('/etc/letsencrypt/live/socket.begingt.com.br/privkey.pem', 'utf8'),
  cert: fs.readFileSync('/etc/letsencrypt/live/socket.begingt.com.br/fullchain.pem','utf8')
};

server = https.createServer(options, app);
//Fim - Produção

//Ambiente Local
/*
var http, server;
http = require("http");
server = http.createServer(app);
*/
//Fim - Ambiente Local

var io = socketio(server);
var clients = [];
/**
 * Initialize Server
 */
app.use(
    bodyParser.urlencoded({
      extended: true
    })
);
app.use(bodyParser.json());
server.listen(8888, function() {
  console.log("Servidor Rodando na Porta 8888");
});
/**
 * Página de Teste
 */
app.get("/", function(req, res) {
  res.send("Servidor Rodando...");
});

/**
 * Titulo Selecionado
 */

app.post("/tituloSelecionado", function(req, res) {
  console.log("/tituloSelecionado");

  var params = req.body;
  console.log(params);
  clients.forEach(function(v, i) {
    var listen = v[0];
    // Dispara evento para os clientes
    listen.emit("tituloSelecionado", params);
  });
  res.send();
});

/**
 * Titulo Finalizado
 */

app.post("/finalizaTituloSelecionado", function(req, res) {
  var params = req.body;
  console.log("/finalizaTituloSelecionado");
  console.log(params);
  clients.forEach(function(v, i) {
    var listen = v[0];
    listen.emit("finalizaTituloSelecionado", params);
  });
  res.send();
});

/**
 * Libera título para acordo
 */
app.post("/liberarTituloSelecionado", function(req, res) {
  var params = req.body;

  console.log("/liberarTituloSelecionado");
  console.log(params);

  clients.forEach(function(v, i) {
    var listen = v[0];
    listen.emit("liberarTituloSelecionado", params);
  });
  res.send();
});

// Recebe conexão dos usuários no servidor
io.on("connection", function(client) {
  // Adicionado clientes
  client.emit("welcome", null, function(data) {
    clients.push([this, data.id]);
  });

  console.log("Um cliente conectado");
});
