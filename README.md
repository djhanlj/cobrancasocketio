## INSTALAÇÃO 

#### Node 
	- Fazer a instalação do node.js de acordo com o SO utilizado na mquina
	
#### Clonar o projeto 
	- Realizar o clone do projeto na pasta desejada
	- Instalar as dependências do node
	- Acessar o arquivo index.js e configurar de acordo com o ambiente (**Produção/Local**) - Trecho comentado no código

#### Service Systemd
	- Alterar o arquivo socket-begin.service
	- Na linha ExecStart adicionar o caminho completo do index.js
	- criar um link simbólico para o caminho do service:
	sudo ln -s /home/ubuntu/cobrancasocketio/socket-begin.service /etc/systemd/system/socket-begin.service
	- Atualizar o daemon do systemd: 
	sudo systemctl daemon-reload
	- Ativar o service que foi linkado:
	sudo systemctl enable sockert-begin.service
	- Rodar o service:
	sudo systemctl start socket-begin 
#### Executar o servidor
	- Local: node index.js 		
		*obs: utilize essa opção para acompanhar as informações do servidor*
	- Produção: nohup node index.js &	

## CERTIFICADOS SSL - Necessário caso se deseje utilizar o HTTPS

#### Alterar grupo de segurança do EC2 na AWS para permitir acesso a porta 80

#### Instalar acme.sh
	- sudo su
	- cd ~
	- curl https://get.acme.sh | sh -s email=desenvolvimento@ieptbto.com.br
	- apt-get install socat
	- cd .acme.sh/

#### Criar certificados para o domínio
	- ./acme.sh --issue --apache -d socket.begingt.com.br
	- cp /root/.acme.sh/socket.begingt.com.br/socket.begingt.com.br.key /home/ubuntu/privkey.pem
	- cp /root/.acme.sh/socket.begingt.com.br/fullchain.cer /home/ubuntu/fullchain.pem
	- cd /home/ubuntu/
	- sudo chown socket:socket fullchain.pem privkey.pem 

#### Alterar as permissões da pasta para aplicação ter acesso aos arquivos
	- sudo chown ubuntu:ubuntu fullchain.pem privkey.pem 
	
#### Alterar grupo de segurança do EC2 na AWS para remover acesso a porta 80



